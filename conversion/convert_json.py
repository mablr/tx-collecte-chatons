#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import argparse
from textwrap import dedent
from data import conversion


#   Parser :
def makeParser() -> argparse.ArgumentParser():
    """Permet de créer le argparse initial"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input",
        type=argparse.FileType("r"),
        help=dedent(
            """Fichier JSON à convertir
            """
        ),
    )
    parser.add_argument(
        "-o",
        "--output",
        type=argparse.FileType("w"),
        required=False,
        help="""Fichier dans lequel exporter les données converties""",
    )
    return parser


# Partie JSON
def convertDegradability(nb: int) -> str:
    """Convertir une durée exprimée en int en un texte explicite"""
    if nb == 0:
        return "Jamais"
    elif nb < 7:
        return "1 semaine"
    elif nb < 6*30.5:
        return "6 mois"
    else:
        return "12 mois"


def showNode(str):  # Debug
    """Affiche l'endroit du parcours de graphe"""
    pass
    # print(str)


def debug(str):
    """Tout est dit"""
    print(str)


def convertDict(d: dict, context: str = None) -> dict:
    """Convertir un dictionnaire de notre format json en format entraide"""
    showNode(f"D : {d}, {context}")
    return {
        ck: cv
        for (ck, cv) in [
            (convertKey(k, context), convertValue(v, k))
            for (k, v) in d.items()
        ]
        if ck is not None and cv is not None
    }


def convertList(l: list, context: str = None) -> list:
    """Convertir une liste de notre format json en format entraide"""
    showNode(f"L : {l}, {context}")
    return [
        cv
        for cv in [convertValue(item, context) for item in l]
        if cv is not None
    ]


def convertKey(k: str, context: str = None) -> str:
    """Convertir une clé en format entraide grâce au dictionnaire 'conversion'
    """
    showNode(f"K : {k}, {context}")
    keyList = conversion["keys"].get(context, None)
    if keyList is not None:
        return keyList.get(k, None)

    return None


def convertStr(v: str, context: str = None) -> str:
    """Convertir une str en format entraide grâce au dictionnaire 'conversion'
    """
    showNode(f"S : {v}, {context}")
    if context == "url" or context == "name":
        return v
    # elif context == "consent":
    #     return v
    # elif context == "node_link":
    #     return v
    elif context == "type" or context == "software" or context == "account":
        value = conversion["values"][context].get(v, None)
        if value is None:
            print(f"{context} '{v}' non-existant")
        return value

    return None


def convertInt(v: int, context: str = None) -> str:
    """Convertir un entier en format entraide"""
    if context == "degradability":
        return convertDegradability(v)
    elif context == "weight":
        return str(v)

    return None


def convertValue(val, context: str = None):
    """Convertisseur json générique, point d'entrée de la conversion"""
    showNode(f"V : {val}, {context}")
    if type(val) is int:
        return convertInt(val, context)
    if type(val) is str:
        return convertStr(val, context)
    elif type(val) is dict:
        return convertDict(val, context)
    elif type(val) is list:
        return convertList(val, context)
    else:
        return None


# Partie explosion des Chatons par services
def exploseChatonL(l: list) -> list:
    """Explosion des listes de Chatons"""
    return [
        chaton
        for converted_cs in [       # Une liste par Chaton en théorie
            exploseChatonServices(chaton)
            for chaton in l
        ]
        for chaton in converted_cs  # Une seule liste à la fin
    ]


def exploseChatonD(c: dict) -> list:
    """Explosion des dictionnaires de Chatons"""
    services = c['services']
    chaton = c.copy()
    del chaton['services']
    return [
        makeChatonService(chaton, service)
        for service in services
    ]


def exploseChatonServices(d) -> list:
    """Explosion des Chatons selon leur type"""
    if type(d) is dict:
        return exploseChatonD(d)
    elif type(d) is list:
        return exploseChatonL(d)
    else:
        return []


def makeChatonService(c: dict, service: dict) -> dict:
    """Passage des champs du service aux champs du chaton"""
    nc = c.copy()
    # Parce que le consentement et le lien d'édition n'existent pas dans
    # notre schema, on met des valeurs bidons
    nc['consent'] = '1'
    nc['edit_link'] = 'https://chatons.org/fr/node/0000/edit'
    for (k, v) in service.items():
        nc[k] = v

    return nc


#   Nodeification du json :
def nodeify(liste_chaton: list) -> dict:
    """Convertir en une structure node la liste de Chatons"""
    return {
        "nodes": [
            {"node": chaton}
            for chaton in liste_chaton
        ]
    }

#   Vérification que toutes les valeurs du dictionnaire sont dans l'ordre
def addMissingValues(services: list) -> list:
    for service in services:
        for field, value in conversion["expected_fields"].items():
            if field not in service:
                if type(value) is str:
                    service[field] = "Valeur par défaut"
                elif type(value) is int:
                    service[field] = "1"
    return services

#   Synthèse des conversions :
def convertSchema(input) -> dict:
    """Maxi-conversion entre le schéma de la Tx et le schéma entraide"""
    return nodeify(                             # Structure node
        addMissingValues(                       # Tous les champs nécessaires
            exploseChatonServices(              # Explosion par service
                convertValue(input, "chaton")   # Conversion valeur/clé JSON
            )
        )
    )
