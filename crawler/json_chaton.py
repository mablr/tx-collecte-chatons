#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import urllib.request as req
from url_normalize import url_normalize
from jsonschema import validate, ValidationError
import logger
from textwrap import dedent
import argparse


# Classe d'exception utilisée par urlToJSON
class NormalizeException(Exception):
    pass


class UrlOpeningException(Exception):
    pass


def urlToJSON(url: str):
    """Copie le json contenu dans un fichier à l'adresse url
    Renvoie une liste ou un dictionnaire selon les données récupérées
    """
    # Tentative de normalisation de l'URL
    try:
        pretty_url = url_normalize(url)
    except:
        raise NormalizeException()

    # Tentative d'ouverture du fichier pointé
    try:
        ans = req.urlopen(pretty_url)
    except:
        raise UrlOpeningException()

    # Tentative de lecture du fichier téléchargé
    data = json.loads(ans.read().decode("utf-8"))
    return data


class Chaton:
    def __init__(self, key, data):
        self.key = key
        self.data = data

    def __eq__(self, other):
        """Égalité entre Chatons.
        Un Chaton ne peut être égal à aucun autre si son attibut de clé \
        n'existe pas.
        Deux chatons sont égaux si leurs clés ont la même valeur.
        """
        if self.key is None or other.key is None:
            return False
        try:
            return self.data[self.key] == other.data[other.key]
        except KeyError:
            return False

    def __hash__(self):
        return hash(self.key)

    def __str__(self):
        """Permet de retourner (s'il existe) le nom du Chaton"""
        try:
            return str(self.data["name"])
        except KeyError:
            return "Nom inconnu"

    def sameData(self, other) -> bool:
        """Renvoie vrai ssi a==b et a.data == b.data"""
        return self == other and self.data == other.data

    def sameDifferentData(self, other) -> bool:
        """Renvoie vrai iff a==b et a.data != b.data"""
        return self == other and self.data != other.data


# Classe les nouveaux chatons par rapport au précédent fichier
def getUpdatedAndUntouched(old: set, new: set) -> (set, set):
    """Retourne les chatons mis à jour et intouchés entre deux listes

    Un chaton est intouché si la valeur de la clé (si elle existe) est la même\
    et si les données sont les mêmes en tout point.
    Un chaton est mis à jour si la valeur de la clé est la même et les \
    données sont différentes.
    """
    updated = set()
    untouched = set()

    for a in old:
        for b in new:
            if b.sameData(a):
                untouched.add(b)
            elif b.sameDifferentData(a):
                updated.add(b)
    return updated, untouched


def showChatonListIfVerbose(msg: str, chatons: set, log: logger.Log = None):
    """Affiche le nom des Chatons contenus dans un set donné"""
    nb = len(chatons)
    if nb > 0:
        log.info(msg + " : " + str(nb))
        for chaton in chatons:
            # log.debug(str(chaton))
            name = str(chaton)
            if type(log) is logger.Log:
                log.infoVerbose(name, 1)
            else:
                print(f"  - {name}")


def showChaton(chaton):  # debug
    print(f"C {str(chaton)}")


# Pour valider les anciens chatons selon l'actuel schéma si besoin
def validateOldChatons(chatons: set, schema) -> (list, list):
    """Renvoie deux set : les chatons valides/invalides selon un schéma donné
    """
    valid = set()
    invalid = set()
    for chaton in chatons:
        try:
            validate(instance=chaton, schema=schema)
        except ValidationError:
            invalid.add(chaton)
        else:
            valid.add(chaton)
    return valid, invalid


def makeParser() -> argparse.ArgumentParser():
    """Permet de créer le argparse initial"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-u",
        "--update-only",
        action="store_true",
        required=False,
        help=dedent(
            """\
            Ne permet pas la supression d'un CHATON d'une exécution sur
            l'autre, les anciennes données sont conservées. Seuls l'ajout et
            la modification sont présents dans le fichier mis à jour.
            """
        ),
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        required=False,
        help="""Affiche plus d'informations""",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        required=False,
        help=dedent(
            """Fichier JSON dans lequel exporter les données collectées
            (Par défaut : INPUT.json)
            """
        ),
    )
    parser.add_argument(
        "-s",
        "--schema",
        type=argparse.FileType("r"),
        required=False,
        help=dedent(
            """Schéma JSON auquel confronter les données si
            donné en argument
            """
        ),
    )
    parser.add_argument(
        "-i",
        "--input",
        type=argparse.FileType("r"),
        required=True,
        help="""Liste des adresses des fichiers JSON décrivant les CHATONS""",
    )
    return parser
