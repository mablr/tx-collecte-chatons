// Chargement de la liste des CHATONS et peuplement de la section appropriée
function loadChatons () {
  const originFrame = document.getElementById('chatons');
  const content = originFrame.contentWindow.document.body.childNodes[0].innerHTML;
  const chatons = JSON.parse(content);

  const section = document.getElementById('list');
  displayChatons(chatons, section);
}

// Fonction qui permet d'afficher les informations de plusieurs Chatons dans la section voulue
function displayChatons (chatons, section) {
  // Remise à zéro de la section
  section.innerHTML = '';
  // Itération sur tous les Chatons
  for (let i = 0; i < chatons.length; i++) {
    // Création d'un conteneur
    const chaton = document.createElement('article');

    // Toutes les propriétés pertinentes
    const name = document.createElement('h3');
    name.textContent = chatons[i].name;
    chaton.appendChild(name);

    const info = document.createElement('div');
    info.setAttribute('class', 'chatonInfo');
    chaton.appendChild(info);

    if (typeof chatons[i].url !== 'undefined') {
      populateDiv(info, 'Site web du Chaton', chatons[i].url);
    }
    if (typeof chatons[i].description !== 'undefined') {
      populateDiv(info, 'Description', chatons[i].description);
    }
    if (typeof chatons[i].structure !== 'undefined') {
      populateDiv(info, 'Structure', chatons[i].structure);
    }
    if (typeof chatons[i].economic_model !== 'undefined') {
      populateDiv(info, 'Modèle économique', chatons[i].economic_model.join(', '));
    }
    if (typeof chatons[i].public !== 'undefined') {
      populateDiv(info, "Public auquel s'adresse le Chaton", chatons[i].public.join(', '));
    }

    if (typeof chatons[i].services !== 'undefined') {
      const services = document.createElement('h4');
      services.textContent = 'Liste des services proposés';
      chaton.appendChild(services);
      const servicesTable = document.createElement('table');
      generateTable(servicesTable, chatons[i].services);
      chaton.appendChild(servicesTable);
    }

    if (typeof chatons[i].creation_date !== 'undefined') {
      populateDiv(info, 'Date de création', chatons[i].creation_date);
    }
    if (typeof chatons[i].address !== 'undefined') {
      populateDiv(info, 'Adresse', chatons[i].address);
    }
    if (typeof chatons[i].perimeter !== 'undefined') {
      populateDiv(info, "Périmètre d'action", chatons[i].perimeter);
    }

    section.appendChild(chaton);
  }
}

// Une fonction pour créer plus facilement les champs des Chatons
function populateDiv (div, key, value) {
  const element = document.createElement('p');
  element.setAttribute('class', 'chatonInfo');
  element.textContent = key + ' : ' + stringBeautify(value);
  div.appendChild(element);
}

// Pour générer des tableaux
function generateTable (table, data) {
  generateTableHead(table, Object.keys(data[0]));
  for (const element of data) {
    const row = table.insertRow();
    for (const key in element) {
      const cell = row.insertCell();
      const text = document.createTextNode(stringBeautify(element[key]));
      cell.appendChild(text);
    }
  }
}
// Avec des headers
function generateTableHead (table, data) {
  const thead = table.createTHead();
  const row = thead.insertRow();
  for (const key of data) {
    const th = document.createElement('th');
    const text = document.createTextNode(stringBeautify(key));
    th.appendChild(text);
    row.appendChild(th);
  }
}

// Fonctions qui permettent d'afficher ou cacher la liste de tous les Chatons
function showChatons () {
  const section = document.getElementById('list');
  // En affichant la section
  section.hidden = false;
  const button = document.getElementById('toggle_display');
  // Et en changeant le comportement du bouton
  button.setAttribute('value', 'Cacher la liste des CHATONS');
  button.setAttribute('onclick', 'hideChatons()');
  return false;
}

function hideChatons () {
  const section = document.getElementById('list');
  section.hidden = true;
  const button = document.getElementById('toggle_display');
  button.setAttribute('value', 'Afficher tous les CHATONS');
  button.setAttribute('onclick', 'showChatons()');
  return false;
}
