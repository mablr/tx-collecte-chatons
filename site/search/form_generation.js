// Chargement du schéma depuis la page de recherche
function loadSchema () {
  // Repérage de la frame gui contient le schéma
  const originFrame = document.getElementById('schema');
  // Récupération du contenu de la frame
  const content = originFrame.contentWindow.document.body.childNodes[0].innerHTML;
  // Conversion en JSON
  const schema = JSON.parse(content);

  // Génération du formulaire
  generateForm(schema);
}

// Pour générer le formulaire de recherche grâce au schéma (pour connaître les avleurs possibles)
function generateForm (schema) {
  // On trouve le formulaire
  const form = document.getElementById('form');

  // Affichage de l'en-tête
  let criteria = document.createElement('h3');
  criteria.textContent = 'Structure du Chaton cherché';
  form.appendChild(criteria);
  // Et génération des cases à cocher
  generateInput(true, form, 'structure', schema.definitions.structure.enum);

  criteria = document.createElement('h3');
  criteria.textContent = 'Services recherchés';
  form.appendChild(criteria);
  generateInput(false, form, 'service', schema.definitions.service_type.enum);

  criteria = document.createElement('h3');
  criteria.textContent = 'Modèle économique souhaité';
  form.appendChild(criteria);
  generateInput(false, form, 'economic_model', schema.definitions.economic_model.enum);

  criteria = document.createElement('h3');
  criteria.textContent = 'Publics qui peuvent utiliser le Chaton';
  form.appendChild(criteria);
  generateInput(false, form, 'public', schema.definitions.public.enum);

  const submit = document.createElement('input');
  submit.setAttribute('type', 'button');
  submit.setAttribute('onclick', 'query()');
  submit.setAttribute('value', 'Chercher');
  form.appendChild(submit);
}

// Génération des cases à cocher
function generateInput (singleValued, form, key, values) {
  // Les éléments qui vont nous servir
  let input = document.createElement('input');
  let label = document.createElement('label');
  let lb = document.createElement('br');

  // S'il n'y a qu'une seule valeur possible, on ajoute la possibilité de ne pas se prononcer
  if (singleValued) {
    // Création de l'input proprement dit
    input.setAttribute('type', 'radio');
    input.setAttribute('name', key);
    input.setAttribute('value', 'any');
    form.appendChild(input);

    // Et du label
    label = document.createElement('label');
    label.textContent = 'Peu importe';
    form.appendChild(label);
    lb = document.createElement('br');
    form.appendChild(lb);
  }

  // Génération de tous les choix
  for (let i = 0; i < values.length; i++) {
    input = document.createElement('input');
    if (singleValued) {
      input.setAttribute('type', 'radio');
    } else {
      input.setAttribute('type', 'checkbox');
    }
    input.setAttribute('name', key);
    input.setAttribute('value', values[i]);
    form.appendChild(input);

    label = document.createElement('label');
    label.textContent = stringBeautify(values[i]);
    form.appendChild(label);
    lb = document.createElement('br');
    form.appendChild(lb);
  }
}
