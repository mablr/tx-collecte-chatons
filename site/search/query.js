// Cœur de la page, fonction qui compare ce qu'il y a dans le formulaire et le json des CHATONS
function query () {
  // Afficher l'emplacement des résultats
  const section = document.getElementById('result');
  section.hidden = false;

  // Chargement des informations des CHATONS
  const originFrame = document.getElementById('chatons');
  const content = originFrame.contentWindow.document.body.childNodes[0].innerHTML;
  const chatons = JSON.parse(content);

  // Chargement des données du formulaire
  const elements = document.getElementById('form').elements;

  // Au départ, tous les CHATONS sont considérés comme satisfaisant potentiellement la requête
  let candidates = chatons;

  // On itère dans toutes les options du formulaire
  for (let item = 0; item < elements.length; item++) {
    // Si une valeur spéciale est demandée
    if (elements[item].checked && elements[item].value !== 'any') {
      // On considère qu'aucun Chaton ne remplit les critères
      const candidatesTmp = [];
      // Et, selon le champ considéré, le traitement diffère
      if (elements[item].name === 'structure') {
        for (let cand = 0; cand < candidates.length; cand++) {
          if (candidates[cand].structure === elements[item].value) {
            // Pour la structure, tout Chaton avec cette forme juridique passe le test
            candidatesTmp.push(candidates[cand]);
          }
        }
      } else if (elements[item].name === 'service') {
        for (let cand = 0; cand < candidates.length; cand++) {
          for (let serv = 0; serv < candidates[cand].services.length; serv++) {
            if (candidates[cand].services[serv].type === elements[item].value) {
              // Dès que le service est trouvé dans la liste des services du Chaton, on peut passer au Chaton suivant
              candidatesTmp.push(candidates[cand]);
              break;
            }
          }
        }
      } else if (elements[item].name === 'economic_model') {
        for (let cand = 0; cand < candidates.length; cand++) {
          if (candidates[cand].economic_model.includes(elements[item].value)) {
            candidatesTmp.push(candidates[cand]);
          }
        }
      } else if (elements[item].name === 'public') {
        for (let cand = 0; cand < candidates.length; cand++) {
          if (candidates[cand].public.includes(elements[item].value)) {
            candidatesTmp.push(candidates[cand]);
          }
        }
      }
      // Pour examiner le prochain critère, on part de la liste des Chatons qui ont validé cette étape
      candidates = candidatesTmp;
    }
  }

  // On change l'affichage de la section des résultats pour montrer les Chatons trouvés
  const resultSection = document.getElementById('candidates');
  displayChatons(candidates, resultSection);

  return false;
}
