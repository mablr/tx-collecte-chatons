######################################################
#      Ceci est le fichier contenant les urls        #
# Les lignes commentées commencent par un croisillon #
######################################################

# 42l
https://42l.fr/.well-known/services.json
# Deblan
https://chatons.deblan.org/services.json
# Colibris Outils Libres
https://colibris-outilslibres.org/services.json
# Picasoft
https://picasoft.net/picasoft.json
# Exemple de Chaton qui a tous les services nécessaires au fonctionnement du site entraide.chatons.org
http://chatons.picasoft.net/fiches/exemple-services-entraide.json
