# Réflexions sur le formatage idéal des données dans un modèle JSON

Sur cette page, nous tenterons de répondre à la question :

Quelles conventions adopter pour les clés et pour les valeurs du [schéma](schema.md) ?

## Les clés

La question ne se pose pas vraiment pour les clés. On a deux options :

* Soit on met les clefs français, et le document est fait uniquement pour un public francophone. On perd la possibilité de [transformer les fiches en JSON-LD](jsonld).
* Soit on met les clés en anglais, respectant ainsi des standards implicites et explicites, permettant aisément de LD-ifier le tout.

Sur ce sujet, la réponse est vite tranchée en faveur de clés en anglais.

## Les valeurs

La question est de nature plus polémique. Il existe trois options principales :

### Tout en anglais

Les valeurs des champs sont des étiquettes en langue anglaise référençant un concept.

```json
"public": {  
   "description": "The public to whomst the Chaton is aimed at",  
   "type": "string",  
   "enum": ["all", "members", "customers",   "private_circle"]  
}  
```  

Avantages :

* Format très générique et adaptable aisément
* Explicite le fait uqe ce sont des étiquettes, pas des valeurs, et qu'il faut les recopier scrupuleusement

Inconvénients :

* Nécessité de refaire tout le schéma
* Nécessite des fichiers de dictionnaire et une fonction convertissant une valeur (`all`) en du français lisible (« Tou⋅te⋅s ») sur le site web
* Nécessité de maîtriser une plus grande quantité de concepts dans une autre langue pour les personnes chargées de maintenir le code
* Éventuellement moins compatible avec la volonté politique francophone des Chatons

### Un système d'étiquettes en français

Les valeurs des champs sont des étiquettes en langue française référençant un concept.

```json
"public": {  
   "description": "La liste des publics auxquels s'adresse un Chaton ou un service",  
   "type": "string",  
   "enum": ["tou.te.s", "adherent.e.s", "client.e.s", "cercle_prive"]  
}  
```

Avantages :

* Le système de description fonctionne actuellement sur ce principe, et cela suffit pour une preuve de concept
* Les étiquettes sont explicites et ne contiennent que des caractères « simples » pour être parsées
* Il suffit de quelques transformations (peu orthodoxes) pour passer d'une étiquette à un groupe nominal en français approximatif pour l'affichage sur le site web : `cercle_prive` devient facilement `Cercle prive` par substitution de caractères

Inconvénients :

* Le mélange sémantique entre étiquette et valeur est mal vu
* Difficile d'évoluer
* Le niveau de simplification des valeurs n'est pas évident
* Est désagréable à lire pour un humain sans être plus facilement manipulable par une machine

### Tout en français

Les valeurs des clés ne sont plus des étiquettes mais bien des valeurs qui seront utilisées telles quelles pour l'affichage, avec leur lot de caractères accentués, d'espaces et de points médians.

```json
"public": {  
   "description": "La liste des publics auxquels s'adresse un Chaton ou un service",  
   "type": "string",  
   "enum": ["tou⋅te·s", "adhérent⋅e⋅s", "client⋅e⋅s", "cercle privé"]  
}  
```

Avantages :

* Pas besoin de fonction de conversion entre étiquette et valeur
* Termes explicites
* C'est le choix qui a été fait pour entraide.chatons.org, l'export en ce format peut donc se faire de manière transparente

Inconvénients :

* Espaces et caractères spéciaux à traiter
* Moins facile à copier à la main (erreurs, accessibilité des caractères spéciaux) par une personne écrivant son fichier json.\
  Si le json est généré automatiquement, ce dernier point perd de son importance
* Nécessité de refaire tout le schéma

## Les descriptions

La description étant l'équivalent d'un commentaire dans du code, une explicitation du quoi, du pourquoi et du comment dans un schéma, il aura été convenu qu'elles soient en français véritable, et pas en une version ASCII-friendly bâtarde entre deux concepts.
