# Le crawler

## Fonctionnement
Le crawler est un outil permettant de centraliser des données JSON contenues dans des fichiers hébergés en ligne représentant chacun un Chaton. Chaque fichier explicite les activités et les services du Chaton, qui pourront être rassemblés dans un gros fichier JSON qui sera à son tour exploité pour les recherches.

Le [fichier central](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/liste/CHATONS_list.txt) stocké sur le Framagit contient une liste d'URL. Le crawler essaie pour chaque URL d'extraire le contenu et vérifie s'il s'agit bien d'un fichier JSON, que ce JSON est éventuellement valide selon un [schéma](https://framagit.org/bertille/tx-collecte-chatons/-/blob/master/schema/CHATONS_schema-0.0.2.json) et que le Chaton décrit n'est pas déjà dans la liste. Une fois tous les critères de validité remplis, l'URL est ajouté à une liste. Si l'option de mise à jour seulement est passée au crawler, celui-ci va rajouter les Chatons supprimés entre deux générations pour ne pas perdre d'information.

Enfin, un fichier [JSON final](http://chatons.picasoft.net/exports/chatons.json) est produit, le rendant utilisable pour [l'outil de recherche](http://chatons.picasoft.net/search/recherche.html). Tout le déroulement du processus est consigné dans un fichier de [log](http://chatons.picasoft.net/logs/log.txt).

## Utilisation
(Une documentation plus complète est disponible sur le [Framagit](https://framagit.org/bertille/tx-collecte-chatons/-/tree/master/crawler))

Le crawler s'appelle en ligne de commande en précisant le fichier d'entrée, celui de sortie et celui du schéma.
```
crawler.py [-h] [-u] [-v] [-o OUTPUT] [-s SCHEMA] -i INPUT
```
- `-i, --input INPUT` : Liste d'URL pointant vers les JSON des Chatons
- `-o, --output OUTPUT` : Fichier JSON dans lequel sont exportées toutes les informations récoltées.
- `-s, --schema SCHEMA` : Fichier optionnel, si donné les jsons des Chatons doivent être valides pour être ajoutés à `OUTPUT`
- `-v, --verbose` : Affiche plus d'informations dans la console
- `-u, --update-only` : Mode « mise à jour seulement », permet de garder les Chatons qui seraient supprimés entre deux exécutions successives (par exemple si un Chaton a un problème avec son json pendant la procédure de mise à jour).
